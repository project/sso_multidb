SSO MultiDB
--------------

Jeremy Isett

Current implementation is for very specific use case.

Requirements
--------------
* clean urls
* profile module with special field

TODO
--------------
 * on logout of slave - redirect to master to logout
 * fix issue with multi-anon sessions
 * use cases - all user/login to redirect to multi_login (once)

RoadMap
--------------
 * currently, mapping uids from Master to Slave depends on the profile module
   and is mapped by hand. 
   * mapping should be automated, or a cleaner admin form created
      * could be a suggest box between usernames
   *  




